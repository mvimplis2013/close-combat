import configparser

def main():
    config = configparser.ConfigParser()

    config["DEFAULT"] = {
        "ServerAliveInterval": "45",
        "Compression": "yes", 
        "CompressionLevel": "9"
    }

    config["bitbucket.org"] = {}
    config["bitbucket.org"]["User"] = "hg"
    config["bitbucket.org"]["Password"] = "top-secret"

    with open("exampple.ini", "w") as configfile:
        config.write(configfile)
        
  
