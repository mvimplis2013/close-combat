from setuptools import setup

setup( 
  name="closecombat",
  version="1.0.0",
  description="Python3 module to collect and display configuration data from INI files",
  keywords="configuration INI",
  # Project's main page 
  url="https://gitlab.com/mvimplis2013/close-combat",
  # Author details
  author="Miltos K. Vimplis", 
  author_email="mvimblis@gmail.com",
  # Choose your license
  license="MIT",
  classifiers=[
    "Programing Language :: Python :: 3.5",
    "Operating System :: Linux", 
    "Environment :: Cosnole"
  ],
  packages=["combat"],
  install_requires=["configparser"], 
  entry_points={
    "console_scripts": [
      "combat=combat.dagger:main",
     ],
  }
)